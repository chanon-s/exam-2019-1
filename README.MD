<div style="page-break-before: always">

### โจทย์

จงเขียนโปรแกรมแสดงอัตราค่าตั๋วเดินทางตามตารางที่กำหนด

### คำชี้แจง

1.  กำหนดให้ภาษา PHP, JavaScript หรือตามที่ผู้คุมสอบอนุญาต
2.  ให้กำหนดตัวแปรชื่อ **source** เก็บชื่อสถานีเริ่มต้น
3.  ให้กำหนดตัวแปรชื่อ **destination** เก็บชื่อสถานีปลายทาง
4.  ให้แสดงผลลัพธ์ตามที่เห็นสมควร

### ตารางค่าตั๋วเดินทาง

<table border="1" cellpadding="10" cellspacing="0">

<tbody>

<tr>

<th>สถานี</th>

<th>A</th>

<th>B</th>

<th>C</th>

<th>D</th>

<th>E</th>

<th>F</th>

<th>G</th>

<th>H</th>

<th>I</th>

<th>J</th>

</tr>

<tr>

<th>A</th>

<td></td>

<td>10</td>

<td>20</td>

<td>30</td>

<td>40</td>

<td>50</td>

<td>60</td>

<td>70</td>

<td>80</td>

<td>100</td>

</tr>

<tr>

<th>B</th>

<td>10</td>

<td></td>

<td>10</td>

<td>20</td>

<td>30</td>

<td>40</td>

<td>50</td>

<td>60</td>

<td>70</td>

<td>90</td>

</tr>

<tr>

<th>C</th>

<td>20</td>

<td>10</td>

<td></td>

<td>10</td>

<td>20</td>

<td>30</td>

<td>40</td>

<td>50</td>

<td>60</td>

<td>80</td>

</tr>

<tr>

<th>D</th>

<td>30</td>

<td>20</td>

<td>10</td>

<td></td>

<td>10</td>

<td>20</td>

<td>30</td>

<td>40</td>

<td>50</td>

<td>70</td>

</tr>

<tr>

<th>E</th>

<td>40</td>

<td>30</td>

<td>20</td>

<td>10</td>

<td></td>

<td>10</td>

<td>20</td>

<td>30</td>

<td>40</td>

<td>60</td>

</tr>

<tr>

<th>F</th>

<td>50</td>

<td>40</td>

<td>30</td>

<td>20</td>

<td>10</td>

<td></td>

<td>10</td>

<td>20</td>

<td>30</td>

<td>50</td>

</tr>

<tr>

<th>G</th>

<td>60</td>

<td>50</td>

<td>30</td>

<td>20</td>

<td>40</td>

<td>10</td>

<td></td>

<td>10</td>

<td>20</td>

<td>40</td>

</tr>

<tr>

<th>H</th>

<td>70</td>

<td>60</td>

<td>50</td>

<td>40</td>

<td>30</td>

<td>20</td>

<td>10</td>

<td></td>

<td>10</td>

<td>30</td>

</tr>

<tr>

<th>I</th>

<td>80</td>

<td>70</td>

<td>60</td>

<td>50</td>

<td>40</td>

<td>30</td>

<td>20</td>

<td>10</td>

<td></td>

<td>20</td>

</tr>

<tr>

<th>J</th>

<td>100</td>

<td>90</td>

<td>80</td>

<td>70</td>

<td>60</td>

<td>50</td>

<td>40</td>

<td>30</td>

<td>20</td>

<td></td>

</tr>

</tbody>

</table>

### ตัวอย่างการทำงาน

*   กำหนดสถานี "C" เป็นต้นทาง (ตัวแปร **source** = "C")
*   กำหนดสถานี "F" เป็นปลายทาง (ตัวแปร **destination** = "F")
*   แสดงผลลัพธ์ค่าตั๋วเท่ากับ 30

</div>