<?php

get_cost('A', 'B');
function get_cost($source_input, $destination_input)
{
    $a = 0;
    $b = 0;
    $source = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
    $destination = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
    for ($i = 0; $i < sizeof($source); $i++) {
        if ($source_input == $source[$i]) {
            $a = $i;
        }
        if ($destination_input == $destination[$i]) {
            $b = $i;
        }
    }
    echo ($source_input == 'J' || $destination_input == 'J') && $source_input != $destination_input ? (abs($b - $a) + 1) * 10 : abs($b - $a) * 10;

}

?>