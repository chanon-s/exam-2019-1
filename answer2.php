<?php
/**
 * Created by PhpStorm.
 * User: Chanon
 * Date: 1/15/2019
 * Time: 3:27 PM
 */

function find_index($needle, $array)
{
    $found = -1;
    foreach ($array as $index => $value) {
        if ($needle == $value) {
            $found = $index;
            break;
        }
    }
    return $found;
}

function main()
{
    $source = 'J';
    $destination = 'I';

    $stName = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

    $fareTable = [
        [null, 10, 20, 30, 40, 50, 60, 70, 80, 100],
        [10, null, 10, 20, 30, 40, 50, 60, 70, 90],
        [20, 10, null, 10, 20, 30, 40, 50, 60, 80],
        [30, 20, 10, null, 10, 20, 30, 40, 50, 70],
        [40, 30, 20, 10, null, 10, 20, 30, 40, 60],
        [50, 40, 30, 20, 10, null, 10, 20, 30, 50],
        [60, 50, 40, 30, 20, 10, null, 10, 20, 40],
        [70, 60, 50, 40, 30, 20, 10, null, 10, 30],
        [80, 70, 60, 50, 40, 30, 20, 10, null, 20],
        [100, 90, 80, 70, 60, 50, 40, 30, 20, null],
    ];


    $iSource = find_index($source, $stName);
    $iDestination = find_index($destination, $stName);

    echo 'Fare = ' . ($fareTable[$iSource][$iDestination]);
}

main();